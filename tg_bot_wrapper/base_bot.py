import hashlib
from urllib.parse import urlparse
from urllib.request import urlopen
import telebot
from telebot import types
import os, traceback, inspect, json, time, logging
from .decorators import command
from .buttons import ButtonText, ButtonUrl
from importlib import import_module
import redis
from pprint import pprint as pp
from telebot import apihelper

logger = logging.getLogger(__name__)
DEBUG_MODE = bool(os.getenv('DEBUG'))
DATA_DEBUG_MODE = bool(os.getenv('DATA_DEBUG'))

R = redis.Redis(host=os.getenv('REDIS_HOST') or 'localhost',
                port=int(os.getenv('REDIS_PORT', 0)) or 6379,
                db=3)

l = logging.getLogger('urllib3.connectionpool')
l.setLevel(logging.ERROR)


class TelegramBot(object):
    show_error_on_unknown_command = True
    unknown_command_error_text = 'Command "{command}" unknown'
    PARSER_MARKDOWN = 'Markdown'
    PARSER_HTML = 'HTML'
    redis_key = 'tgbot'
    redis_callback_key = f'{redis_key}:callback'
    redis_button_key = f'{redis_key}:button'
    proxy = None

    def __init__(self, token, async_=False, offline_mode=False, debug_mode=False, proxy_url=None):
        super().__init__()
        self.DEBUG = debug_mode or DEBUG_MODE
        self.DATA_DEBUG = DATA_DEBUG_MODE
        self.OFFLINE_MODE = offline_mode
        if self.DEBUG:
            print('Debug mode is active!', flush=True)
        if proxy_url:
            apihelper.proxy = {'https': proxy_url}
        elif self.proxy:
            apihelper.proxy = {'https': self.proxy}
        self.token = token
        if async_:
            self.bot = telebot.AsyncTeleBot(token)
        else:
            self.bot = telebot.TeleBot(token)
        self.message = None
        self._tg_commands = command.wrap(self)

        if not self.check_telegram_server_is_available():
            print('Telegram server not available. Offline mod is on', flush=True)
            self.OFFLINE_MODE = True
        if self.OFFLINE_MODE:
            print('OFFLINE MODE IS ENABLED')

        @self.bot.callback_query_handler(func=lambda call: True)
        def callback_inline(call):
            self.message = call.message
            self.raw_inline_callback(call)

        @self.bot.message_handler(content_types=["text"])
        def receive(message):
            if self.DATA_DEBUG:
                print(f'RAW:[{message.chat.id}]: {message.text}', flush=True)
                pp(message.json)
                try:
                    print('User:', self.message.chat.username, flush=True)
                except:
                    print('error get user', flush=True)
            self.raw_receive(message)

    @classmethod
    def is_enabled(cls):
        return True

    def raw_receive(self, message):
        """
        Raw receive simple text message
        """

        self.message = message
        if self.show_error_on_unknown_command:
            if message.text.strip().startswith('/') and not message.text[1:] in self._tg_commands:
                self.send_message(self.unknown_command_error_text.format(command=message.text.strip().split()[-1]))
                return
        self._notify_callbacks()
        self.on_receive()

    def send_error_unknown_command(self):
        self.send_message(self.unknown_command_error_text.format(command=self.message.text.strip().split()[-1]))

    @classmethod
    def _data_to_hash(cls, data):
        """
        Convert data to hash
        :return: str
        """
        return hashlib.md5(data.encode()).hexdigest()[:7]

    def _create_keyboard(self, buttons, chat_id, row_width=3):
        """
        Create Keyboard from Buttons. Save callbacks and arguments to buffer
        :param buttons: [Button, ...]
        :return: InlineKeyboardMarkup
        """
        if not buttons:
            return None
        def create_btn(btn):
            data = btn.json_data()
            data['callback'] = self._method_to_str(data['callback'])
            jdata = json.dumps(data)
            data_hash = self._data_to_hash(jdata)
            R.set(f'{self.redis_button_key}:{chat_id}:{data_hash}', jdata, ex=60 * 60 * 24)
            return data_hash
        keyboard = types.InlineKeyboardMarkup()
        keyboard.row_width = row_width
        for btn in buttons:
            if isinstance(btn, (list, tuple)):
                row = []
                for sub_btn in btn:
                    if isinstance(sub_btn, ButtonText):
                        row.append(types.InlineKeyboardButton(text=sub_btn.text, callback_data=create_btn(sub_btn)))
                    elif isinstance(sub_btn, ButtonUrl):
                        row.append(types.InlineKeyboardButton(text=sub_btn.text, url=sub_btn.url))
                if row:
                    keyboard.row(*row)
            if isinstance(btn, ButtonText):
                # data = btn.json_data()
                # data['callback'] = self._method_to_str(data['callback'])
                # jdata = json.dumps(data)
                # data_hash = self._data_to_hash(jdata)
                # R.set(f'{self.redis_button_key}:{chat_id}:{data_hash}', jdata, ex=60*60*24)
                # if data_hash not in self._callback_args:
                #     self._callback_args[data_hash] = data
                callback_button = types.InlineKeyboardButton(text=btn.text, callback_data=create_btn(btn))
                keyboard.add(callback_button)
            elif isinstance(btn, ButtonUrl):
                url_button = types.InlineKeyboardButton(text=btn.text, url=btn.url)
                keyboard.add(url_button)
        return keyboard

    def process_updates(self, json_string):
        # logger.debug(f'Update... {self.token}')
        update = telebot.types.Update.de_json(json_string)
        return self.bot.process_new_updates([update])

    def send_message(self, text=None, chat_id=None, parse_mode=None, buttons=None, callback=None,
                     callback_args=None, callback_kwargs=None, add_message_argument=False, button_row_width=3,
                     no_url_preview=False):
        if not text or not text.strip():
            logger.error('Empty message can not be sent')
            return
        chat_id = chat_id or self.message.chat.id
        if self.OFFLINE_MODE:
            print(f'Offline fake send: {text}', flush=True)
            traceback.print_exc()
            return
        logger.debug(f'User: {chat_id}, text: {text}')
        # try to register callback
        if (callable(callback) or isinstance(callback, str)) and chat_id:
            self._register_next_step_callback(callback, chat_id, add_message_argument=add_message_argument,
                                              args=callback_args, kwargs=callback_kwargs)
        # create buttons
        if buttons:
            if not isinstance(buttons, (list, tuple)):
                logger.error(f'"buttons" argument must be iterable or None: {type(buttons)}')
                buttons = None
            buttons = self._create_keyboard(buttons, chat_id, button_row_width)
        # send message
        res = self.bot.send_message(chat_id, text, reply_markup=buttons, parse_mode=parse_mode,
                                    disable_web_page_preview=no_url_preview)
        logger.debug(f'Telegram Server Response:\n{res}')
        if self.DATA_DEBUG:
            print(f'Telegram Server Response:\n{res}', flush=True)
        return res

    # callbacks

    def _notify_callbacks(self):
        callback_data = R.get(f'{self.redis_callback_key}:{self.message.chat.id}')
        if callback_data:
            print(callback_data, flush=True)
            R.delete(f'{self.redis_callback_key}:{self.message.chat.id}')
            try:
                callback_data = json.loads(callback_data.decode())
            except Exception as e:
                logger.error(f'Error callback data decode:{e} ({callback_data})')
                return
            callback_name = callback_data.get('callback')
            func = getattr(self, callback_name, None) or self._load_callable(callback_name)
            if not func:
                logger.error(f'Callback "{callback_name}" not found')
                return
            if not callable(func):
                logger.error(f'Object {callback_name} is not callable')
                return
            # execute
            try:
                method_kwargs = callback_data.get('kwargs') or {}
                if callback_data.get('add_message_argument', False):
                    method_kwargs['message'] = self.message
                method_args = callback_data.get('args') or []
                res = func(*method_args, **method_kwargs)
                if res:
                    print('Callback result:', res, flush=True)
            except Exception as e:
                traceback.print_exc()
                logger.error(f'Callback error: {e}')

    def _register_next_step_callback(self, func, chat_id, add_message_argument=False, args=None, kwargs=None):
        if not chat_id:
            raise Exception('Callbacks chat_id not set')
        callback_name = self._method_to_str(func)
        # clear old chat callback
        R.delete(f'{self.redis_callback_key}:{chat_id}')
        # callback id
        R.set(f'{self.redis_callback_key}:{chat_id}', json.dumps(dict(
            callback=callback_name,
            args=args, kwargs=kwargs,
            add_message_argument=add_message_argument
        )))
        logger.debug(f'Saved >>>: [{self.redis_callback_key}:{chat_id}]')
        # self.bot.register_next_step_handler_by_chat_id(message.chat.id,
        # lambda m: self._message_callback(m, callback_id))

    def _method_to_str(self, func):
        if not callable(func) and not isinstance(func, str):
            raise Exception(f'Function must be callable or str: {func}')
        if isinstance(func, str):
            f = self._load_callable(func)
            if not f:
                raise Exception(f'Can`t import function "{func}"')
            callback_name = func
        else:
            if func in dict(inspect.getmembers(self, predicate=inspect.ismethod)).values():
                callback_name = func.__name__
            else:
                try:
                    callback_name = f'{func.__module__}.{func.__name__}'
                except Exception as e:
                    raise Exception(f'Can\'t get callable import name {func}: {e}')
        return callback_name

    def _message_callback(self, message, callback):
        self.message = message
        callback()

    def edit(self, text, message_id=None, chat_id=None, buttons=None,
             parse_mode=None, button_row_width=3, no_url_preview=True):
        if self.OFFLINE_MODE:
            print(f'Fake edit in offline mode {chat_id}>{message_id}>{text}')
            return
        self.bot.edit_message_text(chat_id=chat_id or self.message.chat.id,
                                   message_id=message_id or self.message.message_id,
                                   text=text, reply_markup=self._create_keyboard(buttons, chat_id),
                                   parse_mode=parse_mode,
                                   disable_web_page_preview=no_url_preview)

    def delete(self, chat_id=None, message_id=None):
        self.bot.delete_message(chat_id=chat_id or self.message.chat.id,
                                message_id=message_id or self.message.message_id)

    def on_receive(self):
        """
        On Receive callback for simple text message (not commands)
        """
        if self.DATA_DEBUG:
            print(f'Receive "{self.message.text}"', flush=True)
        # self.send_message(self.message.text)

    def raw_inline_callback(self, call):
        """
        Receive button click and call callback with arguments
        :param call:
        """
        # print('CALL', call, flush=True)
        hash_data = call.data
        if not hash_data:
            if self.DEBUG:
                print('No callback data. Maybe bot was restarted?', flush=True)
            return
        callback_data = R.get(f'{self.redis_button_key}:{self.message.chat.id}:{hash_data}')
        if not callback_data:
            if self.DEBUG:
                print('No callback data', flush=True)
            return
        try:
            data = json.loads(callback_data.decode())
        except Exception as e:
            logger.error(f'Error decode callback data: {e}')
            return

        callback_name = data.get('callback')
        if not callback_name:
            logger.error('No callback in button data')
            return
        func = getattr(self, callback_name, None) or self._load_callable(callback_name)
        if not func:
            logger.error(f'Callback "{callback_name}" not found')
            return
        if not callable(func):
            logger.error(f'Object {callback_name} is not callable')
            return
        func(*data.get('args', []), **data.get('kwargs', {}))

    def bind(self):
        """
        Start bot to listening messages if not web hook is set
        """
        # if self.OFFLINE_MODE:
        #     logger.error('Binding not allowed in offline mode')
        #     return
        self.bot.polling(none_stop=True)

    def set_webhook(self, url=None, certificate=None, max_connections=None, allowed_updates=None):
        if self.OFFLINE_MODE:
            logger.error('Webhook operations not allowed in offline mode')
            return
        # check url
        url = url or self.get_webhook_url()
        if not url:
            raise Exception('Webhook not set')
        # check current webhook
        info = self.bot.get_webhook_info()
        # if self.DEBUG:
        #     print(info, flush=True)
        if info.url == url:
            print(f'Webhook already set to "{url}"')
            return
        # reset webhook
        self.delete_webhook()
        self.bot.set_webhook(url, certificate, max_connections, allowed_updates)

    def get_webhook_url(self):
        return

    def delete_webhook(self):
        if self.OFFLINE_MODE:
            logger.error('Webhook operations not allowed in offline mode')
            return
        self.bot.delete_webhook()

    # @route('webhook')
    # def update(self, json_data):
    #     pass

    @classmethod
    def _send_to_multiple_chats_in_background(cls, token, chat_ids, text, format_data=None, parse_mode=None):
        bot = cls(token, async_=False)
        for chat in chat_ids:
            print('Send to', chat, flush=True)
            try:
                if format_data:
                    user_text = text.format(format_data)
                else:
                    user_text = text
            except Exception as e:
                logger.error(str(e))
                continue
            try:
                res = bot.send_message(user_text, chat_id=chat, parse_mode=parse_mode)
                logger.debug(str(res))
            except Exception as e:
                logger.error(str(e))
            time.sleep(0.1)

    @staticmethod
    def check_telegram_server_is_available(timeout=5):
        o = urlparse(telebot.apihelper.API_URL or 'https://api.telegram.org')
        url = f'https://{o.netloc}'
        try:
            code = urlopen(url, timeout=timeout).getcode()
            logger.debug(f'Telegram server response code: {code}')
            return True
        except:
            return False

    @classmethod
    def _load_callable(cls, name):
        try:
            module_path, class_name = name.rsplit('.', 1)
        except ValueError:
            return
        module = import_module(module_path)
        try:
            return getattr(module, class_name)
        except AttributeError:
            return

