
class ButtonText:
    def __init__(self, text, callback, *args, **kwargs):
        self.text = text
        self.callback = callback
        self.args = args
        self.kwargs = kwargs

    def json_data(self):
        return dict(
            callback=self.callback,
            args=self.args,
            kwargs=self.kwargs
        )


class ButtonUrl:
    def __init__(self, text, url):
        self.text = text
        self.url = url

