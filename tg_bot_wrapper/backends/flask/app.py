"""
start
export FLASK_APP=tg_bot_wrapper.backends.flask.app.py
export DJANGO_SETTINGS_MODULE=myproject.settings
export TG_DOMAIN=domain.com
export TG_WHPATH=tg_webhook
flask run --host=0.0.0.0 --port=8080
"""
import django
django.setup()
from django.conf import settings
from flask import Flask, request, abort
import logging, json, os, sys
import telebot
from ..tools import get_bot
logger = logging.getLogger(__name__)

domain = os.getenv('TG_DOMAIN')
webhook_path = os.getenv('TG_WHPATH')

if not domain:
    raise Exception('Domain not set')
if not webhook_path:
    raise Exception('Webhook path not set')

url = f'https://{domain}/{webhook_path.strip("/")}/'
routing_path = webhook_path.strip("/")

bot = get_bot(settings.TG_BOT_CLASS_NAME, settings.TG_BOT_TOKEN)
if not bot:
    print(f'Bot not created: {settings.TG_BOT_CLASS_NAME}')
    sys.exit()

print(f'Set webhook: {url}', flush=True)
bot.set_webhook(url)

app = Flask(__name__)


@app.route('/', methods=['POST', 'GET'], defaults={'path': ''})
@app.route('/<path:path>/', methods=['POST', 'GET'])
def webhook(path):
    logger.debug(f'REQUEST PATH: {path}')
    if path == 'stop':
        func = request.environ.get('werkzeug.server.shutdown')
        if func is None:
            raise RuntimeError('Not running with the Werkzeug Server')
        logger.debug('Shutdown API server')
        func()
        return json.dumps({'result': 'OK'})
    if request.method == 'POST' and path == routing_path:
        # print(f'Updates incoming... : {path}', flush=True)
        if bot:
            try:
                # json_string = request.get_json(force=True)
                # update = telebot.types.Update.de_json(json_string)
                # bot.process_updates([update])
                bot.process_updates(request.get_json(force=True))
            except Exception as e:
                logger.error(str(e))
                return f'error: {e}'
        return 'ok'
    else:
        abort(403)
# logger.debug(f'Start Telegram Simple Server: 0.0.0.0:{port}')
# app.run('0.0.0.0', port)
