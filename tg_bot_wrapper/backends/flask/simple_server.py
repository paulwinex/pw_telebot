from flask import Flask, request, abort
import logging, json
import telebot
from urllib.parse import urlparse

logger = logging.getLogger(__name__)


def bind(bot, domain, path, port=80):
    """
    domain = 'https://site.com'
    path = 'tgwebhook'
    port = 8080
    """
    if not domain:
        raise Exception('Domain not set')
    o = urlparse(domain)
    if o.scheme != 'https':
        raise Exception(f'Telegram webhook not support HTTP, user HTTPS: {domain}')
    url = f'https://{o.netloc}/{path.strip("/")}/'
    print(f'Set webhook: {url}', flush=True)
    routing_path = path.strip("/")
    bot.set_webhook(url)

    app = Flask(__name__)

    @app.route('/', methods=['POST', 'GET'], defaults={'path': ''})
    @app.route('/<path:path>/', methods=['POST', 'GET'])
    def webhook(path):
        print(f'PATH: {path}', flush=True)
        if path == 'stop':
            func = request.environ.get('werkzeug.server.shutdown')
            if func is None:
                raise RuntimeError('Not running with the Werkzeug Server')
            logger.debug('Shutdown API server')
            func()
            return json.dumps({'result': 'OK'})
        if request.method == 'POST' and path == routing_path:
            print(f'Updates incoming... : {path}', flush=True)
            try:
                bot.process_updates(request.get_json(force=True))
                return 'ok'
            except Exception as e:
                logger.error(str(e))
                return f'error: {e}'
        else:
            abort(403)
