import logging
logger = logging.getLogger(__name__)



def get_class(name):
    module = __import__(name.rsplit('.', 1)[0], fromlist=[name.split('.')[-1]])
    class_ = getattr(module, name.rsplit('.', 1)[1])
    return class_


def get_bot(class_name, token, async=False):
    bot = None
    bot_class = get_class(class_name)
    if not bot_class:
        raise Exception(f'Bot class not valid "{class_name}"')
    if bot_class.is_enabled():
        bot = bot_class(token, async=async)
    else:
        logger.error('Bot is disabled')
    return bot

