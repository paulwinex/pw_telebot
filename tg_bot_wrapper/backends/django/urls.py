"""
Add this string to main urls.py
>>> path('telegrambot/', include(('tg_bot_wrapper.webhook_backends.django.urls', 'tg_bot_wrapper'), namespace='telegram')),
"""
from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from .views import telegram_webhook

app_name = 'telegram_bot'

urlpatterns = [
    path('telegram_webhook/', csrf_exempt(telegram_webhook), name='telegram_webhook')
]
