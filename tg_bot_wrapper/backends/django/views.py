from django.http import HttpResponse, HttpResponseForbidden
from django.conf import settings
import telebot
import json
import logging
from django.shortcuts import reverse

logger = logging.getLogger(__name__)


def telegram_webhook(request, **kwargs):
    if settings.DEBUG:
        print('Incoming message'.center(50, '*'), flush=True)

    json_string = json.loads(request.body)
    print(json_string, flush=True)
    update = telebot.types.Update.de_json(json_string)
    bot.process_updates([update])
    print('COMPLETE'.center(50, '*'), flush=True)
    return HttpResponse('')
    # global bot
    # if not bot:
    #     return HttpResponse('')
    # if request.META.get('CONTENT_TYPE') != 'application/json':
    #     return HttpResponseForbidden()
    # try:
    #     json_string = json.loads(request.body)
    #     update = telebot.types.Update.de_json(json_string)
    #     bot.process_updates([update])
    # except Exception as e:
    #     print(str(e), flush=True)
    #     logger.error(str(e))
    # return HttpResponse('')


def get_class(name):
    module = __import__(name.rsplit('.', 1)[0], fromlist=[name.split('.')[-1]])
    class_ = getattr(module, name.rsplit('.', 1)[1])
    return class_


def set_telegram_bot_webhook(domain):
    url = reverse('telegrambot:telegram_webhook')
    hook_url = f'https://{domain}{url}'
    bot.set_webhook(hook_url)
    return hook_url


bot = None
bot_class = get_class(settings.TG_BOT_CLASS_NAME)
if not bot_class:
    raise Exception(f'Bot class not valid "{settings.TG_BOT_CLASS_NAME}"')
if bot_class.is_enabled():
    bot = bot_class(settings.TG_BOT_TOKEN, async=True)



# def set_webhook(url, class_name=None, token=None, **kwargs):
#     """
#     Quick set webhook
#     """
#
#     url = reverse('telegrambot:telegram_webhook')
#     class_name = class_name or getattr(settings, 'TG_BOT_CLASS_NAME', False)
#     token = token or getattr(settings, 'TG_BOT_TOKEN')
#     if class_name:
#         try:
#             cls = get_class(class_name)
#         except:
#             logger.error(f'Cant get telegram bot class: {class_name}')
#             return HttpResponse('')
#     else:
#         from tg_bot_wrapper.base_bot import TelegramBot as cls
#     bot = cls(token, **kwargs)
#     return bot.set_webhook(url)
