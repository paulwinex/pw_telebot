
Add to wsgi.py to end

```python
from tg_bot_wrapper.webhook_backends.django.views import set_telegram_bot_webhook
domain = some_method()
# 'domain.com'
url = set_telegram_bot_webhook(domain)
logger.debug('Set telegram webhook to {}'.format(url))

```