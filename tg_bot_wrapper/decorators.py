import inspect
__all__ = ['command']


class _CommandWrapper:

    def __call__(self, *cmds):
        def decorator(func):
            setattr(func, 'tg_command', cmds)
            return func
        return decorator

    @staticmethod
    def wrap(instance):
        commands = []
        func_names = []
        # if instance.DEBUG:
        #     for m in [x for x in inspect.getmembers(instance, predicate=inspect.ismethod)]:
        #         print(f'{ "+" if hasattr(m[1], "tg_command") else "-"} {m[0]}')
        if instance.DEBUG:
            print('Bind Commands:', flush=True)
        for name, obj in inspect.getmembers(instance, predicate=inspect.ismethod):
            if hasattr(obj, 'tg_command'):
                cmds = obj.tg_command
                interception = set(cmds) & set(commands)
                if interception:
                    raise Exception(f'Command duplicate: {", ".join(interception)}')
                commands += cmds
                if obj.__name__ in func_names:
                    raise Exception(f'Function name "{obj.__name__}" already exists')
                func_names.append(obj.__name__)
                if instance.DEBUG:
                    print(f'    {",".join(cmds)} >> {obj.__name__}()')

                def inst_wrapper(msg, f):
                    instance.message = msg
                    return f()
                    # return f(instance)

                wrapped = instance.bot.message_handler(commands=cmds)(
                    lambda msg, f=obj: inst_wrapper(msg, f))
                setattr(instance, obj.__name__, wrapped)
        return commands


command = _CommandWrapper()

# def route(func):
#     return func