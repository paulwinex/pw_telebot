from .decorators import command
from .buttons import ButtonText


class DebugMixin(object):
    """
    Add command "getstat" to show debug information
    """
    @command('getstat')
    def get_status(self):
        self.send_statistic()

    @command('debug')
    def debug_mode(self):
        self.send_debug_config()

    def switch_debug(self, attr, val):
        try:
            setattr(self, attr, val)
        except Exception as e:
            self.edit(f'Error: {e}')
            return
        self.edit('Configure Debug Mode', buttons=self.get_debug_buttons())

    def get_debug_buttons(self):
        return [
            ButtonText(
                text=f'Debug Mode = {self.DEBUG}',
                callback=self.switch_debug,
                attr='DEBUG', val=not self.DEBUG
            ),
            ButtonText(
                text=f'Data Debug Mode = {self.DATA_DEBUG}',
                callback=self.switch_debug,
                attr='DATA_DEBUG', val=not self.DATA_DEBUG
            ),
            ButtonText(
                text=f'Close',
                callback=self.delete,
            )

        ]

    def send_statistic(self):
        self.send_message(text=f'''```Bot Stat:
 User:
   {self.message.chat.username}:{self.message.chat.id}
 Commands: 
   {", ".join(self._tg_commands)}
 Debug:
   {self.DEBUG}
 Data Debug:
  {self.DATA_DEBUG}
            ```''', parse_mode='Markdown')

    def send_debug_config(self):
        if self.debug_commands_is_allowed():
            self.send_message('Configure Debug Mode', buttons=self.get_debug_buttons())
        else:
            self.send_error_unknown_command()

    def debug_commands_is_allowed(self):
        return True
