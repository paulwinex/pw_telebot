from setuptools import setup, find_packages

setup(name='tg_bot_wrapper',
      version='0.3',
      description='Simple OOP wrapper for library pyTelegramBotAPI',
      long_description='Class based wrapper for develop telegram bots',
      classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.6',
      ],
      keywords='telegram bot',
      url='https://gitlab.com/paulwinex/pw_telebott',
      author='Paul Winex',
      author_email='paulwinex@gmail.com',
      license='MIT',
      packages=find_packages(),
      install_requires=[
          'pytelegrambotapi', 'redis'
      ],
      include_package_data=True,
      zip_safe=False)
