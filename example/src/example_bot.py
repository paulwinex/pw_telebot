from tg_bot_wrapper.base_bot import TelegramBot
from tg_bot_wrapper.buttons import ButtonText, ButtonUrl
from tg_bot_wrapper.decorators import command
from tg_bot_wrapper.mixins import DebugMixin

import os

DEBUG_MODE = os.getenv('DEBUG')


class ExampleBot(TelegramBot, DebugMixin):
    proxy = os.getenv('TGPROXY')

    @command('start')
    def start(self):
        """
        Command Example
        """
        self.send_message('OK, lets start!!!')

    @command('test')
    def test(self):
        """
        Command example 2 with buttons
        """
        self.send_message('Select Action:',
                  buttons=[
                      ButtonText(
                          text='Act %s' % x,
                          callback=self.action,
                          action='Action %s' % x
                      ) for x in range(5)

                  ] + [ButtonUrl(text='Go To...', url='https://google.com')]
                          )

    @command('hello')
    def hello(self, *args, **kwargs):
        """
        Callback example
        """
        print('HELLO COMMAND', flush=True)
        self.send_message(text='Hello here!!', callback=self.hello_callback)

    def hello_callback(self, *args):
        print('HELLO CALLBACK', self.message.text, flush=True)

    def action(self, call, action):
        """
        Callback Example 2
        :param call:
        :param action:
        :return:
        """
        print(action, flush=True)
        self.edit(text=f'{action} selected')

    # def send_to_multiple_chats(self, chat_ids, text, format_data=None, parse_mode=None):
    #     send_multiple.delay(self.token, chat_ids, text, format_data, parse_mode)

    # def get_webhook_url(self):
    #     return f'https://{os.getenv("TGDOMAIN")}'


# @job
# def send_multiple(*args, **kwargs):
#     TelegramBot._send_to_multiple_chats_in_background(*args, **kwargs)
#


def get_bot():
    # get token from env
    TOKEN = os.getenv('TOKEN', '').strip()
    if not TOKEN:
        raise Exception('Bot token not set')
    bot = ExampleBot(TOKEN)
    return bot


if __name__ == '__main__':

    bot = get_bot()
    print('Start BOT...', flush=True)

    if bot.check_telegram_server_is_available() and os.getenv('TGDOMAIN'):
        from tg_bot_wrapper.backends.flask import simple_server
        simple_server.bind(bot, bot.get_webhook_url(), 'tgwebhook', os.getenv('TGPORT', 8080))
    else:
        bot.bind()
